import 'package:account_book/common/services/database_service.dart';
import 'package:account_book/common/services/notification_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:account_book/common/services/storage_service.dart';

class Global {

  static Future<void> init() async {
    WidgetsFlutterBinding.ensureInitialized();
    await _initService();
  }

  static Future<void> _initService() async {
    await Get.putAsync(() => StorageService().init());
    await Get.putAsync(() => DatabaseService().init());
    await Get.putAsync(() => NotificationService().init());
  }
}