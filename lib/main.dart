import 'package:account_book/global.dart';
import 'package:account_book/routes/app_pages.dart';
import 'package:account_book/routes/app_routes.dart';
import 'package:bruno/bruno.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

void main() async {
  BrnInitializer.register(
      allThemeConfig: BrnAllThemeConfig(
        commonConfig: BrnCommonConfig(
          brandPrimary: const Color(0xFF47C6A8),
        ),
        appBarConfig: BrnAppBarConfig(
          backgroundColor: const Color(0xFF47C6A8),
          actionsStyle: BrnTextStyle(color: Colors.white)
        ),

  ));
  await Global.init();
  runApp(ScreenUtilInit(
      designSize: const Size(375, 812),
      builder: (context, child) {
    return GetMaterialApp(
      title: "窝囊费",
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        BrnLocalizationDelegate.delegate,
      ],
      supportedLocales: const [Locale('zh', 'CN')],
      getPages: AppPages.routes,
      initialRoute: AppPages.initial,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primaryColor: const Color(0xFF47C6A8),
          floatingActionButtonTheme: const FloatingActionButtonThemeData(
              backgroundColor: Color(0xFF47C6A8)
          )
      ),
    );
  }));
}

