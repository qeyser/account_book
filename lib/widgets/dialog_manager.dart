import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DialogManager {


  static ThemeData get theme {
    var theme = ThemeData.fallback();
    if (Get.context != null) {
      theme = Theme.of(Get.context!);
    }
    return theme;
  }

  static Future<T?> showConfirmDialog<T>({
    String cancel = "取消",
    String confirm = "确定",
    String? title,
    Widget? titleWidget,
    Widget? content,
    Widget? cancelWidget,
    Widget? conformWidget,
    VoidCallback? onConfirm,
    VoidCallback? onCancel,
  }) {
    return Get.dialog<T>(
        AlertDialog(
          titlePadding: const EdgeInsets.all(8),
          contentPadding: const EdgeInsets.all(8),
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10))),
          title: title != null ? Text(title, textAlign: TextAlign.center) : null,
          content: content,
          actionsAlignment: MainAxisAlignment.center,
          actions: [
            Container(
              height: 50,
              width: double.infinity,
              decoration: const BoxDecoration(
                  border: Border(
                      top: BorderSide(
                          color: Color(0xFFE0E0E0)
                      )
                  )
              ),
              child: Row(
                children: [
                  Expanded(child: InkWell(
                    child: const Center(
                      child: Text("取消", style: TextStyle(fontWeight: FontWeight.bold)),
                    ),
                    onTap: () {
                      onCancel?.call();
                      Get.back();
                    },
                  )),
                  const VerticalDivider(width: 1,color: Color(
                      0xFFE0E0E0)),
                  Expanded(child: InkWell(
                    child: Center(
                      child: Text("确定", style: TextStyle(fontWeight: FontWeight.bold, color: theme.primaryColor)),
                    ),
                    onTap: () {
                      onConfirm?.call();
                    },
                  ))
                ],
              ),
            ),
          ],
          // actions: actions, // ?? <Widget>[cancelButton, confirmButton],
          buttonPadding: EdgeInsets.zero,
        )
    );
  }

}