import 'package:flutter/material.dart';

class DropdownBar extends StatefulWidget {

  /// 下拉组件数组
  final List<Widget> children;

  /// 下拉组件高度
  final double height;
  final List<double>? heights;

  /// 下拉时间
  final int milliseconds;
  final DropdownController dropdownController;

  const DropdownBar(
      {Key? key,
        required this.children,
        this.height = 200,
        this.milliseconds = 100,
        required this.dropdownController,
        this.heights})
      : super(key: key);
  
  @override
  State<StatefulWidget> createState() {
    return _DropdownBar();
  }
  
}

class _DropdownBar extends State<DropdownBar> with SingleTickerProviderStateMixin {

  late AnimationController _controller;
  late Animation<double> animation;
  late CurvedAnimation cure;
  bool isShowShadow = false;

  @override
  void initState() {
    super.initState();
    //vsync TickerProvider参数 创建Ticker 为了防止动画消耗不必要的资源
    _controller = AnimationController(
        vsync: this,
        duration: Duration(milliseconds: widget.milliseconds)); //2s
    cure = CurvedAnimation(parent: _controller, curve: Curves.easeIn);

    // 高度变化
    animation = Tween(begin: 0.0, end: widget.height).animate(cure);
    // 动画执行监听
    animation.addStatusListener((status) {
      if (status == AnimationStatus.dismissed) {
        // 动画反向执行完毕
        if (!widget.dropdownController.isShow) {
          setState(() {
            isShowShadow = false;
          });
        }
      } else if (status == AnimationStatus.forward) {
        // 动画正向执行完毕
        setState(() {
          isShowShadow = true;
        });
      }
    });

    widget.dropdownController.addListener(() {
      if (widget.dropdownController.isShow) {
        // 显示
        print('xxxx ${widget.dropdownController.index}');
        double h;
        if (widget.heights != null) {
          /// 下拉菜单高度每个菜单自定义
          h = widget.heights![widget.dropdownController.index];
        } else {
          h = widget.height;
        }
        // 高度变化
        animation = Tween(begin: 0.0, end: h).animate(cure);
        _controller.forward();
      } else {
        _controller.reverse();
      }
      // 刷新筛选数据
      setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();
    widget.dropdownController.dispose();
    _controller.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _MenuBuilder(
          animation: animation,
          child: widget.children[widget.dropdownController.index],
        ),
        isShowShadow
            ? Expanded(
            child: InkWell(
              child: AnimatedBuilder(
                  animation: animation,
                  builder: (context, child) {
                    return Container(
                      width: double.infinity,
                      height: MediaQuery.of(context).size.height,
                      color: Colors.black
                          .withOpacity(animation.value / (widget.height * 3)),
                    );
                  }),
              onTap: () {
                widget.dropdownController.hide();
              },
            ))
            : const SizedBox(),
      ],
    );
  }

}

/// 下拉组件
class _MenuBuilder extends StatelessWidget {
  final Animation<double> animation;
  final Widget child;

  const _MenuBuilder({required this.animation, required this.child});

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: animation,
        builder: (context, child) {
          return Container(
            color: Colors.white,
            height: animation.value,
            child: child,
          );
        },
        child: child);
  }
}


/// 菜单控制器
class DropdownController extends ChangeNotifier {
  // 当前组件是否显示 默认不显示
  bool isShow = false;

  // 显示当前组件title的文本
  String title = "";

  // 显示哪个组件 默认第一个
  int index = 0;

  // 选择组件的title
  int titleIndex = 0;

  /// 更改Title
  changeTitle(int titleIndex, String? title) {
    this.titleIndex = titleIndex;
    this.title = title ?? "";
    hide();
  }

  // 显示 下拉
  show(int index) {
    this.index = index;
    if (!isShow) {
      isShow = true;
    }
    notifyListeners();
  }

  // 隐藏 取消
  hide() {
    isShow = false;
    notifyListeners();
  }
}