import 'package:get/get.dart';

class ChartState {

  // 当前日期
  final _currentDate = DateTime.now().obs;
  // 总金额
  final _totalAmount = 0.0.obs;
  // 总笔数
  final _totalNum = 0.obs;

  //日期类型
  final _dateType = 1.obs;
  final _type = 1.obs;
  final List<double> chartData = <double>[].obs;

  final List<Map<String,dynamic>> countCategory = <Map<String,dynamic>>[].obs;

  set totalAmount(value) => _totalAmount.value = value;
  double get  totalAmount => _totalAmount.value;

  set totalNum(value) => _totalNum.value = value;
  int get totalNum => _totalNum.value;

  set dateType(value) => _dateType.value = value;
  int get dateType => _dateType.value;

  set type(value) => _type.value = value;
  int get type => _type.value;

  set currentDate(value) => _currentDate.value = value;
  DateTime get currentDate => _currentDate.value;

}