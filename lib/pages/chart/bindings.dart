
import 'package:get/get.dart';

import 'index.dart';

class ChartBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ChartController>(() => ChartController());
  }

}