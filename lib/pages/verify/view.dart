import 'package:account_book/common/utils/toast_util.dart';
import 'package:account_book/routes/app_routes.dart';
import 'package:account_book/widgets/number_box_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import './index.dart';

class VerifyPage extends GetView<VerifyController> {
  const VerifyPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfff5f5f5),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text("请验证你的密码"),
              NumberBoxField(
                itemWidth: 50,
                  type: NumberBoxItemType.underline,
                  showText: false,
                  onSubmitted: (value) {
                    bool flag = controller.verifyPassword(value);
                    if (flag) {
                      Get.offAllNamed(AppRoutes.tabBar);
                    } else {
                      ToastUtil.show("密码错误");
                    }
                }
              )
            ],
          )
      ),
    );
  }
}
