
import 'package:get/get.dart';

import 'index.dart';

class VerifyBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<VerifyController>(VerifyController());
  }

}