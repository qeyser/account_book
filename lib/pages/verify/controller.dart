import 'package:account_book/common/constants/storage_key.dart';
import 'package:account_book/common/services/storage_service.dart';
import 'package:get/get.dart';

class VerifyController extends GetxController {


  bool verifyPassword(value) {
    String password = StorageService.instance.getString(StorageKeyConstants.verifyPassword);
    return password == value;
  }
}