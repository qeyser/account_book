import 'package:get/get.dart';

class RemindState {

  final _remindFlag = false.obs;
  final _remindTime = DateTime.now().obs;

  set remindFlag(value) => _remindFlag.value = value;
  bool get remindFlag => _remindFlag.value;

  set remindTime(value) => _remindTime.value = value;
  DateTime get remindTime => _remindTime.value;

}