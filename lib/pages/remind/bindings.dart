
import 'package:get/get.dart';

import 'index.dart';

class RemindBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<RemindController>(RemindController());
  }

}