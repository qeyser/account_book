import 'package:bruno/bruno.dart';
import 'package:flustars_flutter3/flustars_flutter3.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import './index.dart';

class RemindPage extends GetView<RemindController> {
  const RemindPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfff5f5f5),
      appBar: _buildAppBar(),
      body: _buildBody(context),
    );
  }

  BrnAppBar _buildAppBar() {
    return BrnAppBar(
      title: "定时提醒",
    );
  }

  Widget _buildBody(BuildContext context) {
    return Container(
        color: Colors.white,
        child: Column(
          children: [
            BrnBaseTitle(
              title: "定时提醒",
              subTitle: "定时提醒记账",
              customActionWidget: Obx(() => Switch(
                value: controller.state.remindFlag,
                onChanged: (bool value) {
                  controller.setRemindFlag(value);
                },
              )),
            ),
            Obx(() => BrnTextSelectFormItem(
              title: "提醒时间",
              subTitle: '每天${DateUtil.formatDate(controller.state.remindTime, format: "HH:mm")}提醒',
              value: DateUtil.formatDate(controller.state.remindTime, format: "HH:mm"),
              onTap: () {
                BrnDatePicker.showDatePicker(
                  context,
                  dateFormat: 'HH:mm',
                  pickerMode: BrnDateTimePickerMode.time,
                  initialDateTime: controller.state.remindTime,
                  pickerTitleConfig: BrnPickerTitleConfig.Default,
                  onConfirm: (dateTime, list) {
                    controller.setRemindTime(dateTime);
                  },
                  onCancel: () {
                    return false;
                  },
                );
              },
            ))
          ],
        )
    );
  }
}
