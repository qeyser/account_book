import 'package:account_book/pages/tab_bar/index.dart';
import 'package:bruno/bruno.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:account_book/routes/app_routes.dart';



class TabBarPage extends GetView<TabBarController> {
  const TabBarPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetRouterOutlet.builder(
        routerDelegate: Get.rootDelegate,
        builder: (context, delegate, currentRoute) {
          controller.delegate = delegate;
          return Scaffold(
            body: GetRouterOutlet(
              initialRoute: AppRoutes.tabBar + AppRoutes.record,
              anchorRoute: AppRoutes.tabBar,
            ),
            bottomNavigationBar: BrnBottomTabBar(
              currentIndex: controller.state.currentIndex,
              items: getTabBarItem(),
              onTap: _onTabBarItemSelect,
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () async {
                // TODO 临时方案，暂时无法解决跳转到一级页面后再返回数据更新问题
                delegate.toNamed("${AppRoutes.tabBar}/empty");
                await Get.toNamed(AppRoutes.enroll);
                _onTabBarItemSelect(controller.state.currentIndex);
              },
              child: const Icon(Icons.add),
            ),
            floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
            floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
          );
        }
    );
  }

  List<BrnBottomTabBarItem> getTabBarItem() {
    const tabBarList = [
      BrnBottomTabBarItem(
          title: Text("明细"), icon: Icon(Icons.details)),
      BrnBottomTabBarItem(
          title: Text("图表"), icon: Icon(Icons.bar_chart)),
      BrnBottomTabBarItem(title: Text(""), icon: Icon(Icons.add)),
      BrnBottomTabBarItem(
          title: Text("账单"), icon: Icon(Icons.account_balance)),
      BrnBottomTabBarItem(
          title: Text("设置"), icon: Icon(Icons.settings))
    ];
    return tabBarList;
  }

  void _onTabBarItemSelect(index) {
    if (index == 2) {
      return;
    }
    controller.state.currentIndex = index;
    switch(index) {
      case 0:
        controller.delegate?.toNamed(AppRoutes.tabBar + AppRoutes.record);
        break;
      case 1:
        controller.delegate?.toNamed(AppRoutes.tabBar + AppRoutes.chart);
        break;
      case 3:
        controller.delegate?.toNamed(AppRoutes.tabBar + AppRoutes.bill);
        break;
      case 4:
        controller.delegate?.toNamed(AppRoutes.tabBar + AppRoutes.setting);
        break;
      default:
    }
  }

}