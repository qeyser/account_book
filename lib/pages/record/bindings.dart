
import 'package:get/get.dart';

import 'index.dart';

class RecordBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<RecordController>(RecordController());
  }

}