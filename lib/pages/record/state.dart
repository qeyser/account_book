import 'package:account_book/common/entity/history.dart';
import 'package:get/get.dart';

class RecordState {
  // 当前日期
  final _currentDate = DateTime.now().obs;
  // 收入
  final _income = 0.0.obs;
  // 支出
  final _expend = 0.0.obs;
  // 结余
  final _surplus = 0.0.obs;
  //交易数
  final _dealNum = 0.obs;
  final List<HistoryDetail> listData = <HistoryDetail>[].obs;
  set income(value) => _income.value = value;
  double get  income => _income.value;
  set expend(value) => _expend.value = value;
  double get expend => _expend.value;
  set surplus(value) => _surplus.value = value;
  double get surplus => _surplus.value;
  set dealNum(value) => _dealNum.value = value;
  int get dealNum => _dealNum.value;

  set currentDate(value) => _currentDate.value = value;
  DateTime get currentDate => _currentDate.value;

}

class HistoryDetail {
  String date = "";
  String week = "";
  double income = 0.0;
  double expend = 0.0;
  List<History> detail = [];
}