import 'package:account_book/common/entity/history.dart';
import 'package:account_book/pages/record/index.dart';
import 'package:bruno/bruno.dart';
import 'package:flustars_flutter3/flustars_flutter3.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class RecordPage extends GetView<RecordController> {
  const RecordPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(context),
      body: _buildBody(context),
    );
  }

  BrnAppBar _buildAppBar(BuildContext context) {
    return BrnAppBar(
        automaticallyImplyLeading: false,
        title: InkWell(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Obx(() => Text(DateUtil.formatDate(controller.state.currentDate, format: "yyyy年MM月"))),
              const Padding(
                  padding: EdgeInsets.only(left: 2),
                  child: Icon(Icons.keyboard_arrow_down))
            ],
          ),
          onTap: () async {
            BrnDatePicker.showDatePicker(
              context,
              dateFormat: 'yyyy年,MMMM月',
              pickerMode: BrnDateTimePickerMode.date,
              initialDateTime: controller.state.currentDate,
              minuteDivider: 30,
              pickerTitleConfig: BrnPickerTitleConfig.Default,
              onConfirm: (dateTime, list) {
                controller.switchDate(dateTime);
              },
              onCancel: () {
                return false;
              },
            );
          },
        ));
  }

  Widget _buildBody(BuildContext context) {
    return Container(
      color: const Color(0xFFFAFAFA),
      padding: const EdgeInsets.only(left: 15, right: 15, bottom: 30, top: 15),
      child: Column(
        children: [
          _buildCard(),
          Expanded(
              child: _buildList(context))
        ],
      ),
    );
  }

  Widget _buildList(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 15),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: const BorderRadius.all(Radius.circular(8.0)),
          boxShadow: [
            BoxShadow(
                color: const Color(0xff999999).withOpacity(0.2),
                offset: Offset.zero,
                blurRadius: 15.0,
                spreadRadius: 7
            )
          ]),
      child: Obx(() {
        if (controller.state.listData.isEmpty) {
          return BrnAbnormalStateWidget(
            isCenterVertical: true,
            title: "暂无明细！",
            content: "点击下方 + 按钮添加一笔吧",
          );
        }
        return ListView.builder(
            itemCount: controller.state.listData.length,
            itemBuilder: (context1, index) {
              HistoryDetail item = controller.state.listData[index];
              return Column(
                children: [
                  Container(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("${item.date} ${item.week}",
                            style:
                            const TextStyle(fontSize: 14, color: Colors.grey)),
                        Text("收：+${item.income}    支：-${item.expend}",
                            style:
                            const TextStyle(fontSize: 14, color: Colors.grey)),
                      ],
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    decoration: const BoxDecoration(
                        border: Border(
                          top: BorderSide(width: 1, color: Color(0xffeeeeee)),
                          bottom: BorderSide(width: 1, color: Color(0xffeeeeee)),
                        )),
                    child: ListView.builder(
                        itemCount: item.detail.length,
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (context2, index) {
                          History history = item.detail[index];
                          return InkWell(
                            child: Container(
                              padding: const EdgeInsets.all(5),
                              height: 60,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(history.categoryName, style: const TextStyle(color: Color(0xFF333333), fontSize: 18)),
                                      history.remark.isEmpty ? const SizedBox(height: 0) :
                                      Text(history.remark, style: const TextStyle(color: Color(0xFF999999))),
                                    ],
                                  ),
                                  history.type == 1
                                      ? Text("-${history.amount}",
                                      style: const TextStyle(color: Colors.green, fontSize: 22))
                                      : Text("+${history.amount}",
                                      style: const TextStyle(color: Colors.red, fontSize: 22)),
                                ],
                              ),
                            ),
                            onLongPress: () {
                              BrnDialogManager.showConfirmDialog(context,
                                  title: "确定要删除吗？",
                                  message: "注意此操作不可撤消",
                                  cancel: "取消",
                                  confirm: "确定",
                                  onConfirm: () {
                                    controller.removeHistory(history);
                                    Get.back();
                                  },
                                  onCancel: () {
                                    Get.back();
                                  }
                              );
                            },
                          );
                        }),
                  )
                ],
              );
            });
      }),
    );
  }

  Widget _buildCard() {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: const BorderRadius.all(Radius.circular(8.0)),
          boxShadow: [
            BoxShadow(
                color: const Color(0xff999999).withOpacity(0.2),
                offset: Offset.zero,
                blurRadius: 15.0,
                spreadRadius: 7
            )
          ]),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: [
                  Obx(() => Text("+${controller.state.income}",
                      style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: Colors.red))),
                  const Text("收入",
                      style: TextStyle(fontSize: 14, color: Colors.grey)),
                ],
              ),
              Column(
                children: [
                  Obx(() => Text("-${controller.state.expend}",
                      style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: Colors.green))),
                  const Text("支出",
                      style: TextStyle(fontSize: 14, color: Colors.grey)),
                ],
              )
            ],
          ),
          const SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  const Text("共",
                      style: TextStyle(fontSize: 14, color: Colors.grey)),
                  const SizedBox(width: 5),
                  Obx(() => Text("${controller.state.dealNum}",
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20))),
                  const SizedBox(width: 5),
                  const Text("笔",
                      style: TextStyle(fontSize: 14, color: Colors.grey)),
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  const Text("结余",
                      style: TextStyle(fontSize: 14, color: Colors.grey)),
                  const SizedBox(width: 5),
                  Obx(() => Text("${controller.state.surplus}",
                      style:
                      const TextStyle(fontWeight: FontWeight.bold, fontSize: 20))),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
