
import 'package:get/get.dart';

import 'index.dart';

class SafetyBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<SafetyController>(SafetyController());
  }

}