import 'package:get/get.dart';

class SafetyState {

  final _verifyFlag = false.obs;
  final _verifyType = "".obs;
  final _verifyText = "".obs;
  final _verifyPwd = "".obs;

  set verifyFlag(value) => _verifyFlag.value = value;
  bool get verifyFlag => _verifyFlag.value;

  set verifyType(value) => _verifyType.value = value;
  String get verifyType => _verifyType.value;

  set verifyText(value) => _verifyText.value = value;
  String get verifyText => _verifyText.value;

  set verifyPwd(value) => _verifyPwd.value = value;
  String get verifyPwd => _verifyPwd.value;

  final List<VerifyItem> verifyList = [
    VerifyItem(label: "数字密码", value: "1"),
    VerifyItem(label: "手势密码", value: "2"),
    VerifyItem(label: "指纹验证", value: "3"),
    VerifyItem(label: "人脸验证", value: "4")
  ];
}

class VerifyItem {
  String label;
  String value;

  VerifyItem({
    required this.label,
    required this.value
  });
}