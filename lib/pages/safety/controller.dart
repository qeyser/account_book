import 'package:account_book/common/constants/storage_key.dart';
import 'package:account_book/common/services/storage_service.dart';
import 'package:account_book/pages/safety/index.dart';
import 'package:get/get.dart';

class SafetyController extends GetxController {

  final state = SafetyState();

  @override
  void onReady() {
    state.verifyFlag = StorageService.instance.getBool(StorageKeyConstants.verifyFlag);
    state.verifyType = StorageService.instance.getString(StorageKeyConstants.verifyType);
    if (state.verifyType.isNotEmpty) {
      VerifyItem? verifyItem = state.verifyList.firstWhereOrNull((element) => element.value == state.verifyType);
      if (verifyItem!=null) {
        state.verifyText = verifyItem.label;
      }
    }
    String verifyPwd = StorageService.instance.getString(StorageKeyConstants.verifyPassword);
    if (verifyPwd.isNotEmpty) {
      state.verifyPwd = "已设置";
    }
    super.onReady();
  }

  Future<void> switchVerifyFlag(bool flag) async {
    await StorageService.instance.setBool(StorageKeyConstants.verifyFlag, flag);
    state.verifyFlag = flag;
  }

  void switchVerifyType(int selectIndex) {
    VerifyItem verifyItem = state.verifyList[selectIndex];
    state.verifyText = verifyItem.label;
    state.verifyType = verifyItem.value;
  }

  // 设置密码验证
  Future<void> setPassword(value) async {
    await StorageService.instance.setString(StorageKeyConstants.verifyPassword, value);
    await StorageService.instance.setString(StorageKeyConstants.verifyType, state.verifyType);
    state.verifyPwd = "已设置";
  }

  // 设置密码验证
  void setGesturePassword(value) {
    StorageService.instance.setString(StorageKeyConstants.gesturePassword, value);
    StorageService.instance.setString(StorageKeyConstants.verifyType, state.verifyType);
  }
}