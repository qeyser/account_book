import 'package:account_book/common/utils/toast_util.dart';
import 'package:account_book/widgets/number_box_field.dart';
import 'package:bruno/bruno.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import './index.dart';

class SafetyPage extends GetView<SafetyController> {
  const SafetyPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfff5f5f5),
      appBar: _buildAppBar(),
      body: _buildBody(context),
    );
  }

  BrnAppBar _buildAppBar() {
    return BrnAppBar(
      title: "安全设置",
    );
  }

  Widget _buildBody(BuildContext context) {
    return Obx(() {
      List<Widget> childList = [
        BrnBaseTitle(
          title: "开启验证",
          subTitle: "开启后每次进入都要验证",
          customActionWidget: Switch(
            value: controller.state.verifyFlag,
            onChanged: (bool value) {
              controller.switchVerifyFlag(value);
            },
          ),
        ),
      ];
      if (controller.state.verifyFlag) {
        childList.add(BrnTextSelectFormItem(
          title: "验证方式",
          subTitle: '只能选择其中一种',
          value: controller.state.verifyText,
          onTap: () {
            List<BrnMultiDataPickerEntity> list = [];
            for(VerifyItem verifyItem in controller.state.verifyList) {
              list.add(BrnMultiDataPickerEntity(text: verifyItem.label, value: verifyItem.value));
            }
            BrnMultiDataPicker(
              context: context,
              title: '验证方式',
              delegate: BrnDefaultMultiDataPickerDelegate(data: list),
              confirmClick: (list) {
                controller.switchVerifyType(list[0]);
              },
            ).show();
          },
        ));
        switch(controller.state.verifyType) {
          case "1":
            childList.add(_buildVerifyPassword());
            break;
        }
      }
      return Container(
          color: Colors.white,
          child: Column(
            children: childList,
          )
      );
    });
  }

  Widget _buildVerifyPassword() {
    return BrnTextSelectFormItem(
      title: "设置密码",
      subTitle: '',
      hint: "未设置",
      value: controller.state.verifyPwd,
      onTap: () {
        Get.defaultDialog(
            title: "设置密码",
            content: SizedBox(
              height: 40,
              child: NumberBoxField(
                itemWidth: 40,
                showCursor: true,
                showText: false,
                type: NumberBoxItemType.underline,
                onSubmitted: (value) {
                  Get.back();
                  String firstPwd = value;
                  Get.defaultDialog(
                      title: "确认密码",
                      content: SizedBox(
                        height: 40,
                        child: NumberBoxField(
                          itemWidth: 40,
                          showCursor: true,
                          showText: false,
                          type: NumberBoxItemType.underline,
                          onSubmitted: (value) async {
                            if (firstPwd != value) {
                              ToastUtil.show("两次密码输入不一致");
                            } else {
                              await controller.setPassword(value);
                              Get.back();
                              ToastUtil.show("密码设置成功");
                            }
                          },
                        ),
                      )
                  );
                },
              ),
            )
        );
      },
    );
  }
}
