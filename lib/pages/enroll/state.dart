import 'package:account_book/common/entity/category.dart';
import 'package:flustars_flutter3/flustars_flutter3.dart';
import 'package:get/get.dart';

class EnrollState {

  final _amount = "".obs;
  final _createDate = DateUtil.formatDate(DateTime.now(), format: "yyyy-MM-dd").obs;
  final _type = 1.obs;
  final Rx<int?> _categoryId = RxnInt();
  final _categoryName = ''.obs;
  final _remark = "".obs;
  final List<Category> categoryData = <Category>[].obs;

  set amount(value) => _amount.value = value;
  get amount => _amount.value;

  set createDate(value) => _createDate.value = value;
  get createDate => _createDate.value;

  set type(value) => _type.value = value;
  get type => _type.value;

  set categoryId(value) => _categoryId.value = value;
  get categoryId => _categoryId.value;

  set categoryName(value) => _categoryName.value = value;
  get categoryName => _categoryName.value;

  set remark(value) => _remark.value = value;
  get remark => _remark.value;

}