
import 'package:get/get.dart';

import 'index.dart';

class EnrollBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<EnrollController>(EnrollController());
  }

}