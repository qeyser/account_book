import 'package:bruno/bruno.dart';
import 'package:flustars_flutter3/flustars_flutter3.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import './index.dart';

class BillPage extends GetView<BillController> {
  const BillPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(context),
      body: _buildBody(),
    );
  }

  BrnAppBar _buildAppBar(BuildContext context) {
    return BrnAppBar(
        automaticallyImplyLeading: false,
        title: InkWell(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Obx(() => Text(DateUtil.formatDate(controller.state.currentDate, format: "yyyy年"))),
              const Padding(
                  padding: EdgeInsets.only(left: 2),
                  child: Icon(Icons.keyboard_arrow_down))
            ],
          ),
          onTap: () async {
            BrnDatePicker.showDatePicker(
              context,
              dateFormat: 'yyyy年',
              pickerMode: BrnDateTimePickerMode.date,
              initialDateTime: controller.state.currentDate,
              minuteDivider: 30,
              pickerTitleConfig: BrnPickerTitleConfig.Default,
              onConfirm: (dateTime, list) {
                controller.switchDate(dateTime);
              },
              onCancel: () {
                return false;
              },
            );
          },
        ),
      bottom: PreferredSize(
        preferredSize: const Size.fromHeight(150),
        child: SizedBox(
          height: 150,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Obx(() => Text(controller.state.surplus.toStringAsFixed(2), style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20))),
              const Text("结余"),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Column(
                    children: [
                      const Text("支出"),
                      Obx(() => Text(controller.state.expend.toStringAsFixed(2), style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20))),
                    ],
                  ),
                  Column(
                    children: [
                      const Text("收入"),
                      Obx(() => Text(controller.state.income.toStringAsFixed(2), style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20))),
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return Obx(() {
      if (controller.state.tableData.isEmpty) {
        return BrnAbnormalStateWidget(
          isCenterVertical: true,
          title: "暂无数据！",
          content: "点击下方 + 按钮添加一笔吧",
        );
      } else {
        return Container(
          height: double.infinity,
          color: const Color(0xFFFAFAFA),
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Table(
                columnWidths: const {
                  0: FixedColumnWidth(40),
                  1: FlexColumnWidth(1),
                  2: FlexColumnWidth(1),
                  3: FlexColumnWidth(1)
                },
                children: _buildTable()
            ),
          ),
        );
      }
    });
  }

  List<TableRow> _buildTable() {
    List<TableRow> rows = [];
    rows.add(const TableRow(
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    color: Color(0xFFebebeb)
                )
            )
        ),
        children: [
          SizedBox(height: 35,child: Center(child: Text("月份",style:  TextStyle(fontWeight: FontWeight.bold)))),
          SizedBox(height: 35,child: Center(child: Text("收入",style:  TextStyle(fontWeight: FontWeight.bold)))),
          SizedBox(height: 35,child: Center(child: Text("支出",style:  TextStyle(fontWeight: FontWeight.bold)))),
          SizedBox(height: 35,child: Center(child: Text("结余",style:  TextStyle(fontWeight: FontWeight.bold))))
        ]
    ));
    for(int i=1;i<=12;i++) {
      rows.add(TableRow(
          decoration: const BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: Color(0xFFebebeb)
              )
            )
          ),
          children: [
            SizedBox(height: 35,child: Center(child: Text('$i月'.padLeft(3,'0')))),
            const SizedBox(height: 35,child: Center(child: Text("0"))),
            const SizedBox(height: 35,child: Center(child: Text("0"))),
            const SizedBox(height: 35,child: Center(child: Text("0"))),
          ]
      ));
    }

    for(Map<String,dynamic> map in controller.state.tableData) {
      final date = map['date'];
      rows[int.parse(date)] = TableRow(
          decoration: const BoxDecoration(
              border: Border(
                  bottom: BorderSide(
                      color: Color(0xFFebebeb)
                  )
              )
          ),
          children: [
            SizedBox(height: 35,child: Center(child: Text("${map['date']}月"))),
            SizedBox(height: 35,child: Center(child: Text("${map['income'].toStringAsFixed(2)}"))),
            SizedBox(height: 35,child: Center(child: Text("${map['expend'].toStringAsFixed(2)}"))),
            SizedBox(height: 35,child: Center(child: Text("${map['surplus'].toStringAsFixed(2)}"))),
          ]
      );
    }
    return rows;
  }
}