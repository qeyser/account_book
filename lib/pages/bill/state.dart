import 'package:get/get.dart';

class BillState {

  // 当前日期
  final _currentDate = DateTime.now().obs;
  // 收入
  final _income = 0.0.obs;
  // 支出
  final _expend = 0.0.obs;
  // 结余
  final _surplus = 0.0.obs;

  final List<Map<String,dynamic>> tableData = <Map<String,dynamic>>[].obs;

  set currentDate(value) => _currentDate.value = value;
  DateTime get currentDate => _currentDate.value;
  set income(value) => _income.value = value;
  double get  income => _income.value;
  set expend(value) => _expend.value = value;
  double get expend => _expend.value;
  set surplus(value) => _surplus.value = value;
  double get surplus => _surplus.value;

}