
import 'package:get/get.dart';

import 'index.dart';

class BillBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BillController>(() => BillController());
  }

}