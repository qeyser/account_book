import 'package:account_book/common/services/notification_service.dart';
import 'package:account_book/routes/app_routes.dart';
import 'package:bruno/bruno.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import './index.dart';

class SettingPage extends GetView<SettingController> {
  const SettingPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfff5f5f5),
      appBar: _buildAppBar(),
      body: _buildBody(),
    );
  }

  BrnAppBar _buildAppBar() {
    return BrnAppBar(
      automaticallyImplyLeading: false,
      title: "设置",
      // bottom: const PreferredSize(
      //   preferredSize: Size.fromHeight(150),
      //   child: Text(""),
      // ),
    );
  }

  Widget _buildBody() {
    return Container(
        color: Colors.white,
        child: Column(
          children: [
            ListTile(
              leading: const Icon(Icons.book),
              title: const Text("我的账本"),
              trailing: const Icon(Icons.arrow_forward_ios),
              onTap: () {
                Get.toNamed(AppRoutes.myBook);
              },
            ),
            const Divider(height: 1.0,indent: 60.0,color: Color(0xFFebebeb)),
            ListTile(
              leading: const Icon(Icons.category),
              title: const Text("类别设置"),
              trailing: const Icon(Icons.arrow_forward_ios),
              onTap: () {
                Get.toNamed(AppRoutes.category);
              },
            ),
            const Divider(height: 1.0,indent: 60.0,color: Color(0xFFebebeb)),
            ListTile(
              leading: const Icon(Icons.access_time_filled),
              title: const Text("定时提醒"),
              trailing: const Icon(Icons.arrow_forward_ios),
              onTap: () {
                Get.toNamed(AppRoutes.remind);
              },
            ),
            const Divider(height: 1.0,indent: 60.0,color: Color(0xFFebebeb)),
            ListTile(
              leading: const Icon(Icons.lock_open_rounded),
              title: const Text("安全设置"),
              trailing: const Icon(Icons.arrow_forward_ios),
              onTap: () {
                Get.toNamed(AppRoutes.safety);
              },
            ),
          ],
        )
    );
  }
}
