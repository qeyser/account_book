import 'package:account_book/common/entity/book.dart';
import 'package:account_book/common/entity/category.dart';
import 'package:get/get.dart';

class CategoryState {

  final _type = 1.obs;
  final List<Category> listData = <Category>[].obs;

  set type(value) => _type.value = value;
  get type => _type.value;
}