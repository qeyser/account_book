import 'dart:async';

import 'package:account_book/common/entity/category.dart';
import 'package:account_book/common/utils/toast_util.dart';
import 'package:account_book/widgets/dialog_manager.dart';
import 'package:bruno/bruno.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import './index.dart';

class CategoryPage extends GetView<CategoryController> {
  const CategoryPage({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          backgroundColor: const Color(0xfff5f5f5),
          appBar: _buildAppBar(context),
          body: _buildBody(context),
        ));
  }

  BrnAppBar _buildAppBar(BuildContext context) {
    return BrnAppBar(
      title: "类别设置",
      actions: BrnIconAction(
        child: const Icon(Icons.add, color: Colors.black),
        iconPressed: () {

          DialogManager.showConfirmDialog(
              title: "新增类别",
              content: Container(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: TextField(
                    controller: controller.inputController,
                    autofocus: true,
                    decoration: const InputDecoration(
                        border: OutlineInputBorder()
                    )
                ),
              ),
              onConfirm: () async {
                await controller.addCategory();
                ToastUtil.show("新增成功");
                Get.back();
              }
          );
        },
      ),
      bottom: _buildTabBar(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Obx(() {
      if (controller.state.listData.isEmpty) {
        return BrnAbnormalStateWidget(
          isCenterVertical: true,
          title: "暂无数据！",
          content: "您可以点击右上角 + 按钮添加类别",
        );
      } else {
        return Container(
            padding: const EdgeInsets.all(10),
            child: GridView.count(
              crossAxisCount: 3,
              mainAxisSpacing: 12,
              crossAxisSpacing: 20,
              childAspectRatio: 2.0,
              children: _buildGrid(context),
            ));
      }
    });
  }

  TabBar _buildTabBar(BuildContext context) {
    return TabBar(
      tabs: const [Tab(text: "支出"), Tab(text: "收入")],
      onTap: (index) {
        controller.switchType(index);
      },
    );
  }

  List<Widget> _buildGrid(BuildContext context) {
    List<Widget> gridList = [];
    for (Category category in controller.state.listData) {
      gridList.add(InkWell(
        child: Container(
          alignment: Alignment.center,
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(8.0))),
          child: Text(category.name),
        ),
        onTap: () {
          controller.inputController.text = category.name;
          DialogManager.showConfirmDialog(
              title: "修改类别",
              content: Container(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: TextField(
                    controller: controller.inputController,
                    autofocus: true,
                    decoration: const InputDecoration(
                        border: OutlineInputBorder()
                    )
                ),
              ),
              onConfirm: () async {
                category.name = controller.inputController.text;
                await controller.updateCategory(category);
                ToastUtil.show("修改成功");
                Get.back();
              }
          );
        },
        onLongPress: () {
          BrnDialogManager.showConfirmDialog(
              context,
              title: "确定要删除该类别吗？",
              cancel: '取消',
              confirm: '确定',
              barrierDismissible: false,
              onConfirm: () async {
                await controller.removeCategory(category.id!);
                if (context.mounted) {
                  Navigator.pop(context);
                }
              },
              onCancel: () {
                Navigator.pop(context);
              }
          );
        },
      ));
    }
    return gridList;
  }

  Future<void> _showBottomSheet(BuildContext context) async {
    return showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        context: context,
        builder: (BuildContext content) {
          return SingleChildScrollView(
              child: Container(
                  padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).viewInsets.bottom),
                  child: BrnNormalFormGroup(
                    title: "普通分组",
                    children: [
                      BrnTextInputFormItem(
                        title: "类别名称",
                        hint: "请输入",
                        onChanged: (newValue) {
                          BrnToast.show(
                              "点击触发回调_${newValue}_onChanged", context);
                        },
                      ),
                    ],
                  )));
        });
  }
}
