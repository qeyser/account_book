
import 'package:get/get.dart';

import 'index.dart';

class MyBookBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<MyBookController>(MyBookController());
  }

}