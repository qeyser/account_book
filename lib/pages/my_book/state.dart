import 'package:account_book/common/entity/book.dart';
import 'package:get/get.dart';

class MyBookState {

  final List<Book> bookList = <Book>[].obs;

}