import 'dart:async';

import 'package:account_book/common/entity/book.dart';
import 'package:account_book/common/utils/toast_util.dart';
import 'package:account_book/widgets/dialog_manager.dart';
import 'package:bruno/bruno.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import './index.dart';

class MyBookPage extends GetView<MyBookController> {
  const MyBookPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(context),
      body: _buildBody(context),
    );
  }

  BrnAppBar _buildAppBar(BuildContext context) {
    return BrnAppBar(
        title: "我的账本",
        actions: BrnIconAction(
          child: const Icon(Icons.add, color: Colors.black),
          iconPressed: () {
            DialogManager.showConfirmDialog(
              title: "新增账本",
              content: Container(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: TextField(
                  controller: controller.inputController,
                  autofocus: true,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder()
                  )
                ),
              ),
              onConfirm: () async {
                await controller.saveBook();
                ToastUtil.show("新增成功");
                Get.back();
              }
            );
          },
        ));
  }

  Widget _buildBody(BuildContext context) {
    return Obx(() {
      if (controller.state.bookList.isEmpty) {
        return BrnAbnormalStateWidget(
          isCenterVertical: true,
          title: "暂无数据！",
          content: "点击右上角 + 添加一个账本",
        );
      } else {
        return Center(
          child: Obx(() => ListView.separated(
              itemBuilder: _buildListItem,
              separatorBuilder: (BuildContext context, int index) {
                return const Divider(
                  height: 1.0,
                  color: Colors.grey,
                );
              },
              itemCount: controller.state.bookList.length)),
        );
      }
    });
  }

  Widget _buildListItem(BuildContext context, int index) {
    Book book = controller.state.bookList[index];
    return ListTile(
      title: Text(book.name),
      subtitle: Text(book.createDate.toString()),
      leading: const Icon(Icons.book),
      trailing: book.useState == 1
          ? const Text("使用中")
          : TextButton(
              onPressed: () {
                controller.switchBook(book);
              },
              child: const Text("使用")),
      onLongPress: () {
        controller.inputController.text = book.name;
        DialogManager.showConfirmDialog(
            title: "修改账本",
            content: Container(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: TextField(
                controller: controller.inputController,
                autofocus: true,
                decoration: const InputDecoration(
                  border: OutlineInputBorder()
                )
              ),
            ),
            onConfirm: () async {
              book.name = controller.inputController.text;
              await controller.updateBook(book);
              ToastUtil.show("修改成功");
              Get.back();
            }
        );
      },
    );
  }
}
