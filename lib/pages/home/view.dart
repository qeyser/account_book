import 'package:account_book/pages/home/index.dart';
import 'package:account_book/routes/app_routes.dart';
import 'package:bruno/bruno.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomePage extends GetView<HomeController> {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return Container(
      color: Colors.white,
      child: Center(
        child: InkWell(
          child: const Text("每一笔窝囊费都来之不易"),
          onTap: () {
            Get.toNamed(AppRoutes.tabBar);
          },
        ),
      ),
    );
  }
}