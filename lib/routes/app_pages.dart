import 'package:account_book/pages/remind/index.dart';
import 'package:account_book/pages/safety/index.dart';
import 'package:account_book/pages/verify/index.dart';
import 'package:get/get.dart';
import 'package:account_book/pages/home/index.dart';
import 'package:account_book/pages/tab_bar/index.dart';
import 'package:account_book/pages/record/index.dart';
import 'package:account_book/pages/chart/index.dart';
import 'package:account_book/pages/enroll/index.dart';
import 'package:account_book/pages/bill/index.dart';
import 'package:account_book/pages/setting/index.dart';
import 'package:account_book/pages/my_book/index.dart';
import 'package:account_book/pages/category/index.dart';

import '../pages/empty/view.dart';
import 'app_routes.dart';
import 'middleware.dart';

class AppPages {

  AppPages._();

  static const initial = AppRoutes.home;

  // static final RouteObserver<Route> observer = RouteObserver();

  static List<String> history = [];

  static final List<GetPage> routes = [
    GetPage(
        name: AppRoutes.home,
        page: () => const HomePage(),
        binding: HomeBinding(),
        middlewares: [
          VerifyMiddleware()
        ]
    ),
    GetPage(
      name: AppRoutes.tabBar,
      page: () => const TabBarPage(),
      binding: TabBarBinding(),
      participatesInRootNavigator: true,
      preventDuplicates: true,
      children: [
        GetPage(
          name: "/empty",
          page: () => const EmptyPage(),
        ),
        GetPage(
          name: AppRoutes.record,
          page: () => const RecordPage(),
          binding: RecordBinding(),
          transition: Transition.cupertino,
        ),
        GetPage(
          name: AppRoutes.chart,
          page: () => const ChartPage(),
          binding: ChartBinding(),
          transition: Transition.cupertino,
        ),
        GetPage(
            name: AppRoutes.bill,
            page: () => const BillPage(),
            binding: BillBinding(),
          transition: Transition.cupertino,
        ),
        GetPage(
            name: AppRoutes.setting,
            page: () => const SettingPage(),
            binding: SettingBinding(),
            transition: Transition.cupertino,
        ),
      ]
    ),
    GetPage(
        name: AppRoutes.enroll,
        page: () => const EnrollPage(),
        binding: EnrollBinding(),
        transition: Transition.fade,
    ),
    GetPage(
        name: AppRoutes.myBook,
        page: () => const MyBookPage(),
        binding: MyBookBinding()
    ),
    GetPage(
        name: AppRoutes.category,
        page: () => const CategoryPage(),
        binding: CategoryBinding()
    ),
    GetPage(
        name: AppRoutes.remind,
        page: () => const RemindPage(),
        binding: RemindBinding()
    ),
    GetPage(
        name: AppRoutes.safety,
        page: () => const SafetyPage(),
        binding: SafetyBinding()
    ),
    GetPage(
        name: AppRoutes.verify,
        page: () => const VerifyPage(),
        binding: VerifyBinding()
    ),
  ];

}