class AppRoutes {
    /// 首页
    static const home = '/home';
    /// 底部菜单
    static const tabBar = '/tabBar';
    /// 明细
    static const record = '/record';
    /// 图表
    static const chart = '/chart';
    /// 记账
    static const enroll = '/enroll';
    /// 账单
    static const bill = '/bill';
    /// 设置
    static const setting = '/setting';
    /// 我的账本
    static const myBook = '/myBook';
    /// 类别设置
    static const category = '/category';
    /// 定时提醒
    static const remind = '/remind';
    /// 安全设置
    static const safety = '/safety';
    /// 验证
    static const verify = '/verify';
}