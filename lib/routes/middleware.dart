import 'package:account_book/routes/app_routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get_navigation/src/routes/route_middleware.dart';
import '../common/constants/storage_key.dart';
import '../common/services/storage_service.dart';

class VerifyMiddleware extends GetMiddleware {

  @override
  RouteSettings? redirect(String? route) {
      bool flag = StorageService.instance.getBool(StorageKeyConstants.verifyFlag);
      if (flag) {
        return const RouteSettings(name: AppRoutes.verify);
      } else {
        return const RouteSettings(name: AppRoutes.tabBar);
      }
  }
}