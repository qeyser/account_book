class StorageKeyConstants {
  /// 当前账本
  static const String currentBook = "currentBook";

  /// 定时提醒开关
  static const String remindFlag = "remindFlag";

  /// 定时提醒时间
  static const String remindTime = "remindTime";

  /// 验证开关
  static const String verifyFlag = "verifyFlag";

  /// 验证方式
  static const String verifyType = "verifyType";

  /// 验证密码
  static const String verifyPassword = "verifyPassword";

  /// 手势密码
  static const String gesturePassword = "gesturePassword";

}