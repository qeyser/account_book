import 'package:flutter/foundation.dart';

enum UseStateEnum { use, unUse}

extension UseStateExtension on UseStateEnum {
  String get name => describeEnum(this);

  String get displayLabel {
    switch (this) {
      case UseStateEnum.use:
        return '使用中';
      case UseStateEnum.unUse:
        return '未使用';
      default:
        return '其它';
    }
  }
}