import 'package:account_book/common/entity/book.dart';
import 'package:account_book/common/entity/category.dart';
import 'package:sqflite/sqflite.dart';
import 'base_dao.dart';



class CategoryDao extends BaseDao {

  CategoryDao._();
  static CategoryDao? _instance;
  static CategoryDao get instance => _instance ??= CategoryDao._();


  @override
  String tableName = "category";

  @override
  Future<void> onCreate(Database db, int version) async {
     await db.execute("""
      CREATE TABLE $tableName
      (
        id INTEGER PRIMARY KEY,
        name TEXT NOT NULL,
        type INTEGER DEFAULT 1,
        icon TEXT,
        createDate TEXT
      )
    """);

  }

  Future<List<Category>> getListByType(int type) async {
    List<Map<String,dynamic>> list = await database.rawQuery("select * from $tableName where type = ?", [type]);
    return List.generate(list.length, (index) {
      return Category.fromMap(list[index]);
    });
  }

  Future<int> removeById(int id) async {
    return await database.delete(tableName, where: "id = ?", whereArgs: [id]);
  }
}