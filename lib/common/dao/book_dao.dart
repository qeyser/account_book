import 'package:account_book/common/entity/book.dart';
import 'package:sqflite/sqflite.dart';
import 'base_dao.dart';



class BookDao extends BaseDao {

  BookDao._();
  static BookDao? _instance;
  static BookDao get instance => _instance ??= BookDao._();


  @override
  String tableName = "book";

  @override
  Future<void> onCreate(Database db, int version) async {
     await db.execute("""
      CREATE TABLE $tableName
      (
        id INTEGER PRIMARY KEY,
        name TEXT NOT NULL,
        useState INTEGER DEFAULT 0,
        createDate TEXT
      )
    """);

  }

  Future<List<Book>> getAll() async {
    List<Map<String,dynamic>> list = await database.query(tableName);
    return List.generate(list.length, (index) {
      return Book.fromMap(list[index]);
    });
  }
  
  Future<void> switchUseState(int id) async {
    await database.rawUpdate("update $tableName set useState = 0 where useState = 1");
    await database.rawUpdate("update $tableName set useState = 1 where id = ?", [id]);
  }
}