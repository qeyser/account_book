
class Book {

  /// 账本id
  int? id;

  /// 账本名称
  String name;

  /// 使用状态 0 未使用 1 使用中
  int useState;

  String createDate;

  Book({
    this.name = '新建账本',
    this.useState = 0,
    this.createDate = ''
  });


  static Book fromMap(Map<String, dynamic> json) {
    Book entity = Book();
    if (json.isNotEmpty) {
      entity.id = json["id"];
      entity.name = json["name"];
      entity.useState = json["useState"];
      entity.createDate = json["createDate"] ?? '';
    }
    return entity;
  }


  Map<String, dynamic> toMap() => {
    "id": id,
    "name": name,
    "useState": useState,
    "createDate": createDate,
  };

}