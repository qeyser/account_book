
class Category {

  /// 类别id
  int? id;

  /// 类别名称
  String name;

  /// 使用状态 1 支出 2 收入
  int type;

  /// 图标
  String icon;

  String createDate;

  Category({
    this.name = '类别',
    this.type = 1,
    this.icon = '',
    this.createDate = ''
  });


  static Category fromMap(Map<String, dynamic> json) {
    Category entity = Category();
    if (json.isNotEmpty) {
      entity.id = json["id"];
      entity.name = json["name"];
      entity.type = json["type"];
      entity.icon = json["icon"];
      entity.createDate = json["createDate"] ?? '';
    }
    return entity;
  }


  Map<String, dynamic> toMap() => {
    "id": id,
    "name": name,
    "type": type,
    "icon": icon,
    "createDate": createDate,
  };

}