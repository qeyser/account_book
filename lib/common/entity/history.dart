
class History {

  /// 费用id
  int? id;

  int bookId;

  /// 费用id
  int categoryId;

  // 费用名称
  String categoryName;

  /// 金额
  double amount;

  /// 备注
  String remark;

  /// 费用类型 1 支出 2 收入
  int type;

  String createDate;

  History({
    required this.bookId,
    required this.categoryId,
    this.categoryName = '',
    this.amount = 0.0,
    this.remark = '',
    this.type = 0,
    this.createDate = ''
  });


  static History fromMap(Map<String, dynamic> json) {
    History entity = History(bookId: 0, categoryId: 0);
    if (json.isNotEmpty) {
      entity.id = json["id"];
      entity.bookId = json["bookId"];
      entity.categoryId = json["categoryId"];
      entity.categoryName = json["categoryName"];
      entity.amount = json["amount"];
      entity.remark = json["remark"];
      entity.type = json["type"];
      entity.createDate = json["createDate"] ?? '';
    }
    return entity;
  }


  Map<String, dynamic> toMap() => {
    "id": id,
    "bookId": bookId,
    "categoryId": categoryId,
    "categoryName": categoryName,
    "amount": amount,
    "remark": remark,
    "type": type,
    "createDate": createDate,
  };

}